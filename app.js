var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var routes = require('./app-api/routes/index');
var log = require('winston');
var app = express();
var creds = require('./app-api/config/settings')['DATABASE'];


mongoose.connect(creds.uri);

// view engine setup
app.set('views', path.join(__dirname,  'app-client', 'src'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app-client')));

app.use('/', routes);

app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({"message_id" : err.name + ": " + err.message_id});
    }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message_id = err.message_id;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.use(function(err, req, res, next) {
    const status = err.status || 500;
    const loglevel = (status == 500) ? 'error' : 'warn';
    log.log(loglevel, err.message, { req: { body: req.body, ip: req.ip, method: req.method, url: req.originalUrl }, err: err });
    res.status(status);
    if (req.url.startsWith('/'))
        res.json({
            success: false,
            message: err.message
        });
    else
        res.render('error/5xx', {message: err.message})
});
module.exports = app;
