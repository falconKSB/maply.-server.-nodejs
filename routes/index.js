var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');

var ctrlUsers = require('../controllers/users');
var ctrlAuth = require('../controllers/auth');
var ctrlEvents = require('../controllers/events');
var ctrlMaply = require('../controllers/maply');
var ctrlMessages = require('../controllers/messages');
var ctrlUpload = require('../controllers/amazons3');
var ctrlAdmin = require('../controllers/admin');

var auth = jwt({
    secret: "maply",
    userProperty: 'payload'
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/admins/login', ctrlAdmin.login);
router.post('/admins', ctrlAdmin.addAdmin);
router.put('/admins/:id', ctrlAdmin.updateAdmin);
router.get('/admins/:id', ctrlAdmin.getAdmin);
router.post('/admins/list/:page', ctrlAdmin.getList);
router.post('/admins/change-password', ctrlAdmin.changePassword);

router.post('/events', ctrlEvents.addEvent);
router.put('/events/:id', ctrlEvents.addEventUpdate);
router.post('/events/image', ctrlEvents.uploadEventImage);
router.get('/events/:id', ctrlEvents.getEventDetails);
router.delete('/events/:id', ctrlEvents.deleteEvent);
router.get('/events/list/:page', ctrlEvents.getList);

router.post('/s3/upload', ctrlUpload.upload);
router.post('/s3/download', ctrlUpload.download);

router.get('/journeys/journey/:userId/:offset', ctrlMaply.getJourney);
router.post('/journeys/journey/:journeyUserId/view/:userId/:offset', ctrlMaply.getJourneyMessage);
router.put('/journeys//moment/:messageId/user/:userId', ctrlMaply.readMessage);

router.post('/messages', ctrlMessages.addMessage);
router.post('/messages/:id', ctrlMessages.replyMessage);
router.post('/messages/send/:messageId', ctrlMessages.sendMessage);
router.get('/messages/unread/:userId', ctrlMessages.getUnreadMessageCount);
router.post('/messages/sent/:userId', ctrlMessages.getSentMessage);
router.post('/messages/direct/:userId', ctrlMessages.getDirectMessage);
router.post('/messages/:messageId/save/:userId', ctrlMessages.saveMessage);
router.post('/messages/view/:userId', ctrlMessages.readMessage);

router.put('/users', ctrlUsers.updateUser);
router.get('/users/check/:username', ctrlUsers.checkUserName);
router.post('/users/login', ctrlUsers.loginUser);
router.get('/users/notification/:userId', ctrlUsers.getNotification);
router.put('/users/notification', ctrlUsers.updateNotification);
router.post('/users/facebook/:userId', ctrlUsers.uploadFacebookFriends);
router.post('/users/image', ctrlUsers.uploadUserImage);
router.get('/users/search/:userId/:offset', ctrlUsers.searchUser);
router.put('/users/:userId/action', ctrlUsers.userAction);
router.get('/users/:userId/block/:offset', ctrlUsers.getBlockUser);
router.get('/users/:userId/friends', ctrlUsers.getTopFriendList);
router.get('/users/:userId/requests', ctrlUsers.getFriendRequest);
router.get('/users/:userId/friends/:offset', ctrlUsers.getFriendList);
router.get('/users/nearby/:userId', ctrlUsers.getNearByEvent);
router.get('/users/nearby-event/:userId', ctrlUsers.getUserChat);
router.put('/users/push-token', ctrlUsers.updateUserPushToken);
router.put('/users/location', ctrlUsers.updateLocation);
router.put('/users/batteryStatus', ctrlUsers.updateBatteryStatus);
router.get('/users/:id/:messageType/:userId/:offset', ctrlUsers.getUserChat);
router.get('/users/list/:page', ctrlUsers.getList);
router.get('/users/:id', ctrlUsers.getUser);

router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);
router.post('/forgot', ctrlAuth.forgot);


module.exports = router;
