var monarchApp = angular.module('maply', ['ngRoute', 'ngAnimate']);

monarchApp.$inject = ['$http', '$q'];
// configure our routes
monarchApp.config(function($routeProvider) {
    $routeProvider

    .when('/', {
        templateUrl : 'admin-template/index.html',
        controller  : 'indexController'
    })
    .when('/index', {
        templateUrl : 'admin-template/index.html',
        controller  : 'indexController'
    })

});

monarchApp.directive("sparklinechart", function () {

    return {
        restrict: "E",
        scope: {
            data: "@"
        },
        compile: function (tElement, tAttrs, transclude) {
            tElement.replaceWith("<span>" + tAttrs.data + "</span>");
            return function (scope, element, attrs) {
                attrs.$observe("data", function (newValue) {
                    element.html(newValue);
                    element.sparkline('html', { type: 'line', width: '96%', height: '80px', barWidth: 11, barColor: 'blue' });
                });
            };
        }
    };
});

monarchApp.controller('indexController', function($scope, $http) {
    let service = {};
    let crudServiceBaseUrl = "/users/last-week";
    service.get = function () {
        return $http.get(crudServiceBaseUrl).then(
            function (response) {
                $scope.totalWeek = response.data
            },
            function (errResponse) {
                console.log(errResponse)
            }
        );
    };
    service.get();

    crudServiceBaseUrl = "/users/last-day";
    service.totalDay = function () {
        return $http.get(crudServiceBaseUrl).then(
            function (response) {
                $scope.totalDay = response.data
            },
            function (errResponse) {
                console.log(errResponse)
            }
        );
    };
    service.totalDay();


    crudServiceBaseUrl = "/users/last-hour";
    service.totalHour = function () {
        return $http.get(crudServiceBaseUrl).then(
            function (response) {
                $scope.totalHour = response.data
            },
            function (errResponse) {
                console.log(errResponse)
            }
        );
    };
    service.totalHour();
});


