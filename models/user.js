/**
 * Created by Sanchir Kartiev on 2/14/17.
 */

var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var creds = require('../config/settings')['DATABASE'];

var connection = mongoose.createConnection(creds.uri);
autoIncrement.initialize(connection);

var userSchema = mongoose.Schema({
    username: { type: String},
    id: {type: Number},
    facebook_id: { type: String },
    image: { type: String },
    deviceToken: { type: String },
    deviceType : ['Ios', 'Android'],
    location: { type: String },
    fullAddress: { type: String },
    name: { type: String },
    email: { type: String },
    batteryStatus: { type: Number },
    lang: { type: String },
    awsArn: { type: String },
    isPublished: { type: Boolean },
    status: ['Live','Away'],
    offlineHours: { type: Number },
    statusActiveTill: { type: Date, default: Date.now },
    latitude: { type: Number },
    longitude: { type: Number },
    badgeCount: { type: Number },
    statusType: ['None', 'Request', 'Block', 'Friend', 'Live', 'LiveRequest', 'Decline', 'Disconnect'],
    liveType : ['H24', 'H72', 'UntilDisconnect'],
    time: { type: Date, default: Date.now },
    description: { type: String },
    title: { type: String },
    type: { type: String },
    password: { type: String },
    role : ['USER','ADMIN'],
    gender: { type: String },
    birthday: { type: Date, default: Date.now }
});


userSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

userSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
    return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.name,
        exp: parseInt(expiry.getTime() / 1000),
    }, "maply");
};

userSchema.plugin(autoIncrement.plugin, { model: 'User', field: 'id', startAt: 1 });

var User = mongoose.model('User', userSchema);
module.exports = { User : User};
