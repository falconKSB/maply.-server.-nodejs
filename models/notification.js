/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */
var mongoose = require("mongoose");

var notificationSchema = mongoose.Schema({
    maplyNews : {type: Boolean},
    message : {type : Boolean},
    friendRequest : {type: Boolean},
    liveRequest : {type: Boolean},
    liveUpdate : {type: Boolean},
    liveFriendsNearby : {type: Boolean},
    maixmumDistance : {type: Number},
    userId : {type: Number},
    soundNotification : {type: Boolean}
});

module.exports = mongoose.model("Notification", notificationSchema);