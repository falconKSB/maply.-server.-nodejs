/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userChatSchema = mongoose.Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    userBy: {type: Schema.Types.ObjectId, ref: 'User'},
    message: {type: Schema.Types.ObjectId, ref: 'Message'},
    isUnread : {type: Boolean},
    savedTill: { type: Date, default: Date.now },
    isSaved : {type: Boolean},
    isPublished : {type: Boolean}
});

module.exports = mongoose.model("UserChat", userChatSchema);