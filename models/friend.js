/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var friendSchema = Schema({
    userTo: {type: Schema.Types.ObjectId, ref: 'User'},
    userBy: {type: Schema.Types.ObjectId, ref: 'User'},
    friendRequest: ['None', 'Request', 'Block', 'Friend', 'Live', 'LiveRequest', 'Decline', 'Disconnect'],
    liveRequest: ['None', 'Request', 'Block', 'Friend', 'Live', 'LiveRequest', 'Decline', 'Disconnect'],
    liveRequestType: ['H24', 'H72', 'UntilDisconnect'],
    expiryDate: {type: Date, default: Date.now},
    isFacebookFriend: {type: Boolean},
    isInternalFriend: {type: Boolean}
});

var Friend =  mongoose.model("Friend", friendSchema);
module.exports = { Friend : Friend};
