/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var messageSchema = Schema({
    image: { type: String },
    locationId: { type: String },
    locationName: { type: String },
    locationAddress: { type: String },
    latitude: { type: Number },
    longitude : {type: Number},
    text: { type: String },
    journeyId : {type: Number},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    count : {type: Number},
    media: { type: String },
    bannerImage: { type: String },
    viewCount : {type: Number},
    expired : {type: Boolean},
    isSaved : {type: Boolean},
    isPublished : {type: Boolean}
});

messageSchema.methods.getImage = function(){
    if(this.image)
    {
        //TODO To bucket url
        return  "https://s3.amazonaws.com/image-maply/" + this.image;
    }
    return this.image;
};

messageSchema.methods.getMedia = function(){
    if(this.media)
    {
        //TODO To bucket url
        return  "https://s3.amazonaws.com/image-maply/" + this.media;
    }
    return this.media;
};

messageSchema.methods.getBannerImage = function(){
    if(this.bannerImage)
    {
        //TODO To bucket url
        return  "https://s3.amazonaws.com/image-maply/" + this.bannerImage;
    }
    return this.bannerImage;
};

module.exports = mongoose.model("Message", messageSchema);
