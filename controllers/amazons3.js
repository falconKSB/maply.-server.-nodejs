/**
 * Created by Sanchir Kartiev on 2/15/2017.
 */

var s3 = require('s3');
var creds = require('../config/settings')['AWSCONFIG'];


var client = s3.createClient({
    maxAsyncS3: 20,     // this is the default
    s3RetryCount: 3,    // this is the default
    s3RetryDelay: 1000, // this is the default
    multipartUploadThreshold: 20971520, // this is the default (20 MB)
    multipartUploadSize: 15728640, // this is the default (15 MB)
    s3Options: {
        accessKeyId: creds.accessKeyId,
        secretAccessKey: creds.secretAccessKey,
        region: creds.region,
        endpoint: creds.baseURL
    }
});

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.upload = function(req, res) {
    var filePath = req.body.filepath;
    var key = req.body.filename;
    var params = {
        localFile: filePath,
        s3Params: {
            Bucket: creds.bucket,
            Key: key
        }
    };

    var uploader = client.uploadFile(params);
    uploader.on('error', function(err) {
        console.error("unable to upload:", err.stack);
    });
    uploader.on('progress', function() {
        console.log("progress", uploader.progressMd5Amount,
            uploader.progressAmount, uploader.progressTotal);
    });
    uploader.on('end', function() {
        var url = s3.getPublicUrl(creds.bucket, key, creds.region);
        sendJSONresponse(res, 200, {
            "status" : url
        });
    });
};

module.exports.download = function(req, res) {
    var filePath = req.body.filepath;
    var link = req.body.linkPath;
    var params = {
        localFile: filePath,
        s3Params: {
            Bucket: creds.bucket,
            Key: link
        }
    };
    var downloader = client.downloadFile(params);
    downloader.on('error', function(err) {
        console.error("unable to download:", err.stack);
    });
    downloader.on('progress', function() {
        console.log("progress", downloader.progressAmount, downloader.progressTotal);
    });
    downloader.on('end', function() {
        sendJSONresponse(res, 200, {
            "status" : "OK"
        });
    });
};

module.exports.delete = function(req, res) {
    var link = req.body.linkPath;
    var params = {
        localFile: filePath,
        s3Params: {
            Bucket: creds.bucket,
            Key: link
        }
    };
    var downloader = client.downloadFile(params);
    downloader.on('error', function(err) {
        console.error("unable to download:", err.stack);
    });
    downloader.on('progress', function() {
        console.log("progress", downloader.progressAmount, downloader.progressTotal);
    });
    downloader.on('end', function() {
        sendJSONresponse(res, 200, {
            "status" : "OK"
        });
    });
};