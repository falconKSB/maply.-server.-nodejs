/**
 * Created by Sanchir on 2/14/2017.
 */
var MessageService = require('../services/message-service');

module.exports.addMessage =  function(req, res) {
    MessageService.addMessage(req.params.banner_image, req.params.video, req.params.location_id,
                                  req.params.latitude, req.params.longitude, req.params.text,
                                  req.params.location_name, req.params.address, req.params.user_id)
        .then(function (message) {
            res.json(message);
    });
};


module.exports.replyMessage =  function(req, res) {
    MessageService.addMessage(req.params.id, req.params.image, req.params.location_id,
        req.params.latitude, req.params.longitude, req.params.location_name,
        req.params.address, req.params.user_id, req.params.text)
        .then(function (message) {
            res.json(message);
    });
};


module.exports.sendMessage =  function(req, res) {
    MessageService.sendMessage(req.params.messageId, req.body)
        .then(function (message) {
            res.json(message);
    });
};



module.exports.getUnreadMessageCount =  function(req, res) {
    MessageService.getUnreadMessageCount(req.params.messageId)
        .then(function (message) {
            res.json(message);
    });
};

module.exports.getSentMessage =  function(req, res) {
    MessageService.getSentMessage(req.params.messageId)
        .then(function (message) {
            res.json(message);
    });
};

module.exports.getDirectMessage =  function(req, res) {
    MessageService.getDirectMessage(req.params.messageId)
        .then(function (message) {
            res.json(message);
    });
};

module.exports.saveMessage =  function(req, res) {
    MessageService.saveMessage(req.params.messageId, req.params.userId)
        .then(function (message) {
            res.json(message);
    });
};

module.exports.readMessage =  function(req, res) {
    MessageService.readMessage(req.params.userId, req.params.ids)
        .then(function (message) {
            res.json(message);
    });
};