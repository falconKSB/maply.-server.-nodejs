var UserService = require('../services/user-service');

module.exports.updateUser =  function(req, res) {
    UserService.updateUser(req.body)
        .then(function (user) {
           res.json(user);
        });
};

module.exports.checkUserName =  function(req, res) {
    let userName = req.params.username;
    UserService.checkUserName(userName)
        .then(function (isAvailable) {
            if(isAvailable) {
                res.status(200);
                res.send();
            }

            else {
                res.status(409);
                res.send();
            }
    });
};

module.exports.loginUser =  function(req, res) {
    UserService.loginUser(req.body)
        .then(function (user) {
            res.json(user);
    })
};

module.exports.getNotification =  function(req, res) {
    UserService.getNotification(req.params.userId)
        .then(function (notification) {
            res.json(notification);
    });

};

module.exports.updateNotification =  function(req, res) {
    UserService.updateNotification(req.body)
        .then(function (notification) {
            res.json(notification);
    });
};

module.exports.uploadFacebookFriends =  function(req, res) {
    UserService.uploadFacebookFriends(req.params.userId, req.body)
        .then(function (users) {
            res.json(users);
    });
};

module.exports.uploadUserImage =  function(req, res) {
    UserService.uploadUserImage(req.params.image, req.params.id)
        .then(function (userImage) {
            res.json(userImage);
    });
};

module.exports.searchUser =  function(req, res) {
    UserService.searchUser(req.params.userId, req.params.searchUser, req.params.offset)
        .then(function (users) {
            res.json(users);
    })
};

module.exports.userAction =  function(req, res) {
    UserService.userAction(req.params.userId, req.body)
        .then(function (response) {
            res.json(response);
    });
};

module.exports.getBlockUser =  function(req, res) {

};

module.exports.getTopFriendList =  function(req, res) {
    UserService.getTopFriendList(req.params.userId)
        .then(function (friendList) {
            res.json(friendList);
    });
};

module.exports.getFriendRequest =  function(req, res) {
    UserService.getFriendRequest(req.params.userId)
        .then(function (friendRequest) {
            res.json(friendRequest);
    });
};

module.exports.getFriendList =  function(req, res) {
    let userId = req.params.userId;
    let offset = req.params.offset;

    UserService.getFriendRequest(userId)
        .then(function (friendRequest) {
            res.json(friendRequest);
    });
};

module.exports.geNearby =  function(req, res) {
    let userId = req.params.userId;
    let latitude = req.params.latitude;
    let longitude = req.params.longitude;

    UserService.getNearByEvent(userId, latitude, longitude)
        .then(function (userList) {
            res.json(userList);
    });
};

module.exports.getNearByEvent =  function(req, res) {
    let userId = req.params.userId;
    let latitude = req.params.latitude;
    let longitude = req.params.longitude;

    UserService.getNearByEvent(userId, latitude, longitude)
        .then(function (userList) {
            res.json(userList);
    });
};

module.exports.getUserChat =  function(req, res) {
    let offset = req.params.offset;
    UserService.getUserChat(req.params.id, req.params.userId, offset)
        .then(function (messageList) {
            res.json(messageList);
    });
};

module.exports.updateUserPushToken =  function(req, res) {
    UserService.updateUserPushToken(req.body)
        .then(function (user) {
            res.json(user);
    });
};

module.exports.updateLocation =  function(req, res) {
    UserService.updateLocation(req.body)
        .then(function (user) {
            res.json(user);
    });
};

module.exports.updateBatteryStatus =  function(req, res) {
    UserService.updateBatteryStatus(req.body)
        .then(function (user) {
            res.json(user);
    });
};

module.exports.getUserChat =  function(req, res) {
    UserService.getUserChat(req.body)
        .then(function (chat) {
            res.json(chat);
        });
};

module.exports.getList =  function(req, res) {
    UserService.getList(req.body)
        .then(function (chat) {
            res.json(chat);
        });
};

module.exports.getUser =  function(req, res) {
    UserService.getUser(req.params.id)
        .then(function (user) {
            res.json(user);
    });
};

