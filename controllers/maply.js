/**
 * Created by Sanchir Kartiev on 2/14/2017.
 */

var MaplyService  = require('../services/maply-service');


module.exports.getJourney =  function(req, res) {
    MaplyService.getJourney(req.params.userId, req.params.offset)
        .then(function (journey) {
            res.json(journey);
    });
};


module.exports.getJourneyMessage =  function(req, res) {
    MaplyService.getJourneyMessage(req.params.journeyUserId,req.params.userId, req.params.offset)
        .then(function (journey) {
            res.json(journey);
    });
};


module.exports.readMessage =  function(req, res) {
    MaplyService.deleteMessage(req.params.messageId, req.params.userId)
        .then(function (journey) {
            res.json(journey);
    });
};