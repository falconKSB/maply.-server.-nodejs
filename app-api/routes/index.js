var express = require('express');
var router = express.Router();
var path = require('path');
var jwt = require('express-jwt');

var ctrlUsers = require('../controllers/users');
var ctrlAuth = require('../controllers/auth');
var ctrlEvents = require('../controllers/events');
var ctrlMaply = require('../controllers/maply');
var ctrlMessages = require('../controllers/messages');
var ctrlUpload = require('../controllers/amazons3');
var ctrlAdmin = require('../controllers/admin');

var auth = jwt({
    secret: "maply",
    userProperty: 'payload'
});

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname, '../../public/admin-template/index.html'));
});

router.get('/users/last-hour', ctrlUsers.getLastHour);
router.get('/users/last-day', ctrlUsers.getLastDay);
router.get('/users/last-week', ctrlUsers.getLastWeek);

router.post('/admins/login', ctrlAdmin.login);
router.post('/admins', ctrlAdmin.addAdmin);
router.put('/admins/:id', ctrlAdmin.updateAdmin);
router.get('/admins/:id', ctrlAdmin.getAdmin);
router.post('/admins/list/:page', ctrlAdmin.getList);
router.post('/admins/change-password', ctrlAdmin.changePassword);

router.post('/events', ctrlEvents.addEvent);
router.put('/events/:id', ctrlEvents.addEventUpdate);
router.post('/events/image', ctrlEvents.uploadEventImage);
router.get('/events/:id', ctrlEvents.getEventDetails);
router.delete('/events/:id', ctrlEvents.deleteEvent);
router.get('/events/list/:page', ctrlEvents.getList);

router.post('/s3/upload', ctrlUpload.upload);
router.post('/s3/download', ctrlUpload.download);

router.get('/journeys/journey/:user_id/:offset', ctrlMaply.getJourney);
router.post('/journeys/journey/:journeyUserId/view/:user_id/:offset', ctrlMaply.getJourneyMessage);
router.put('/journeys//moment/:messageId/user_id/:user_id', ctrlMaply.readMessage);

router.post('/messages', ctrlMessages.addMessage);
router.post('/messages/:id', ctrlMessages.replyMessage);
router.post('/messages/send/:messageId', ctrlMessages.sendMessage);
router.get('/messages/unread/:user_id', ctrlMessages.getUnreadMessageCount);
router.post('/messages/sent/:user_id', ctrlMessages.getSentMessage);
router.post('/messages/direct/:user_id', ctrlMessages.getDirectMessage);
router.post('/messages/:messageId/save/:user_id', ctrlMessages.saveMessage);
router.post('/messages/view/:user_id', ctrlMessages.readMessage);

router.put('/users', ctrlUsers.updateUser);
router.get('/users/check/:username', ctrlUsers.checkUserName);
router.post('/users/login', ctrlUsers.loginUser);
router.get('/users/notification/:user_id', ctrlUsers.getNotification);
router.put('/users/notification', ctrlUsers.updateNotification);
router.post('/users/facebook/:user_id', ctrlUsers.uploadFacebookFriends);
router.post('/users/image', ctrlUsers.uploadUserImage);
router.get('/users/search/:user_id/:offset', ctrlUsers.searchUser);
router.put('/users/:user_id/action', ctrlUsers.userAction);
router.get('/users/:user_id/block/:offset', ctrlUsers.getBlockUser);
router.get('/users/:user_id/friends', ctrlUsers.getTopFriendList);
router.get('/users/:user_id/requests', ctrlUsers.getFriendRequest);
router.get('/users/:user_id/friends/:offset', ctrlUsers.getFriendList);
router.get('/users/nearby/:user_id', ctrlUsers.getNearByEvent);
router.get('/users/nearby-event/:user_id', ctrlUsers.getUserChat);
router.put('/users/push-token', ctrlUsers.updateUserPushToken);
router.put('/users/location', ctrlUsers.updateLocation);
router.put('/users/batteryStatus', ctrlUsers.updateBatteryStatus);
router.get('/users/:id/:messageType/:user_id/:offset', ctrlUsers.getUserChat);
router.get('/users/list/:page', ctrlUsers.getList);
router.get('/users/:id', ctrlUsers.getUser);

router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);
router.post('/forgot', ctrlAuth.forgot);


module.exports = router;
