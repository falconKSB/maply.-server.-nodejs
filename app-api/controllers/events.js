/**
 * Created by Sanchir Kartiev on 2/14/2017.
 */
var EventService = require('../services/event-service');

module.exports.addEvent = function(req, res) {
    EventService.addEvent(req.body)
        .then(function (event) {
            res.json(event);
    });
};

module.exports.addEventUpdate = function(req, res) {
    EventService.editEvent(req.params.id, req.body)
        .then(function (message) {
            res.json(message);
    });
};


module.exports.uploadEventImage = function(req, res) {
    EventService.uploadEventImage(req.params.image, req.params.id)
        .then(function (event) {
            res.json(event);
    });
};


module.exports.getEventDetails = function(req, res) {
    EventService.getEventDetails(req.params.id)
        .then(function (event) {
            res.json(event);
    });
};

module.exports.deleteEvent =  function(req, res) {
    EventService.deleteEvent(req.params.id)
        .then(function (event) {
            res.json(event);
    });
};

module.exports.getList =  function(req, res) {
    EventService.addEvent(req.body)
        .then(function (event) {
            res.json(event);
    });
};


