/**
 * Created by Sanchir on 2/17/2017.
 */
var AdminService = require('../services/admin-service');

module.exports.login = function(req, res) {
    AdminService.loginUser(req.body)
        .then(function (user) {
            res.json(user);
    });
};

module.exports.addAdmin =  function(req, res) {
    AdminService.addAdmin(req.body)
        .then(function (user) {
            res.json(user);
    });
};

module.exports.updateAdmin = function(req, res) {
    AdminService.updateAdmin(req.params.id, req.body)
        .then(function (user) {
            res.json(user);
    });
};

module.exports.getAdmin = function(req, res) {
     AdminService.getAdmin(req.params.id)
         .then(function (user) {
            res.json(user);
    });
};

module.exports.getList = function(req, res) {
    AdminService.getAdminList(req.params.page, req.params.admin_name)
        .then(function (user) {
            res.json(user);
    });
};

module.exports.changePassword = function(req, res) {
    AdminService.changePassword(req.body)
        .then(function (user) {
            res.json(user);
    });
};
