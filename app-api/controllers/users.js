var UserService = require('../services/user-service');
var ListResponse = require('../responses/list-response');
var User = require('../models/user').User;
module.exports.updateUser =  function(req, res) {
    UserService.updateUser(req.body)
        .then(function (user) {
           res.json(user);
        });
};

module.exports.checkUserName =  function(req, res) {
    let userName = req.params.username;
    UserService.checkUserName(userName)
        .then(function (isAvailable) {
            if(isAvailable) {
                res.status(200);
                res.send();
            }

            else {
                res.status(409);
                res.send();
            }
    });
};

module.exports.loginUser =  function(req, res) {
    UserService.loginUser(req.body)
        .then(function (user) {
            res.json(user);
    })
};

module.exports.getNotification =  function(req, res) {
    UserService.getNotification(req.params.user_id)
        .then(function (notification) {
            res.json(notification);
    });

};

module.exports.updateNotification =  function(req, res) {
    UserService.updateNotification(req.body)
        .then(function (notification) {
            res.json(notification);
    });
};

module.exports.uploadFacebookFriends =  function(req, res) {
    UserService.uploadFacebookFriends(req.params.user_id, req.body)
        .then(function (users) {
            res.json(users);
    });
};

module.exports.uploadUserImage =  function(req, res) {
    UserService.uploadUserImage(req.params.image, req.params.id)
        .then(function (userImage) {
            res.json(userImage);
    });
};

module.exports.searchUser =  function(req, res) {
    let offset;
    if (req.params.offset < 0) {
        offset = 0;
    }
    UserService.searchUser(req.params.user_id, req.params.searchUser, offset)
        .then(function (users) {
            let response = new ListResponse();
            response.list = users;
            response.count = users.length;
            response.next_offset = -1;
            res.json(response);
    })
};

module.exports.userAction =  function(req, res) {
    UserService.userAction(req.params.user_id, req.body)
        .then(function (response) {
            res.json(response);
    });
};

module.exports.getBlockUser =  function(req, res) {
    UserService.getBlockUser(req.params.user_id, req.body)
        .then(function (response) {
            res.json(response);
        });
};

module.exports.getTopFriendList =  function(req, res) {
    UserService.getTopFriendList(req.params.user_id)
        .then(function (friendList) {
            res.json(friendList);
    });
};

module.exports.getFriendRequest =  function(req, res) {
    UserService.getFriendRequest(req.params.user_id)
        .then(function (friendRequest) {
            res.json(friendRequest);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getFriendList =  function(req, res) {
    let userId = req.params.user_id;
    let offset = req.params.offset;
    let keyword = req.params.keyword;
    UserService.getFriendList(userId, offset, keyword)
        .then(function (users) {
            let response = new ListResponse();
            response.list = users;
            response.count = users.length;
            response.next_offset = -1;
            res.json(response);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.geNearby =  function(req, res, next) {
    let userId = req.params.user_id;
    let latitude = req.params.latitude;
    let longitude = req.params.longitude;

    UserService.getNearByEvent(userId, latitude, longitude)
        .then(function (userList) {
            res.json(userList);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getNearByEvent =  function(req, res, next) {
    let userId = req.params.user_id;
    let latitude = req.params.latitude;
    let longitude = req.params.longitude;

    UserService.getNearByEvent(userId, latitude, longitude)
        .then(function (userList) {
            res.json(userList);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getUserChat =  function(req, res, next) {
    let offset = req.params.offset;
    UserService.getUserChat(req.params.id, req.params.user_id, offset)
        .then(function (messageList) {
            res.json(messageList);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.updateUserPushToken =  function(req, res) {
    /*UserService.updateUserPushToken(req.body)
        .then(function (user) {
            res.json(user);
    });
*/
    let response = new User();
    response.message = "Successfully Updated";
    res.json(response);
};

module.exports.updateLocation =  function(req, res, next) {
    UserService.updateLocation(req.body)
        .then(function (user) {
            res.json(user);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.updateBatteryStatus =  function(req, res, next) {
    UserService.updateBatteryStatus(req.body)
        .then(function (user) {
            res.json(user);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getUserChat =  function(req, res, next) {
    UserService.getUserChat(req.body)
        .then(function (chat) {
            res.json(chat);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getList =  function(req, res, next) {
    UserService.getList(req.body)
        .then(function (chat) {
            res.json(chat);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getUser =  function(req, res, next) {
    UserService.getUser(req.params.id)
        .then(function (user) {
            res.json(user);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getLastHour =  function(req, res, next) {
    UserService.getLastHour()
        .then(function (users) {
            res.json(users);
        })
        .catch (function (err) {
            return next(err)
        })
};


module.exports.getLastDay =  function(req, res, next) {
    UserService.getLastDay()
        .then(function (users) {
            res.json(users);
        })
        .catch (function (err) {
            return next(err)
        });
};

module.exports.getLastWeek =  function(req, res, next) {
    UserService.getLastWeek()
        .then(function (users) {
            res.json(users);
        })
        .catch (function (err) {
            return next(err)
        })
};

