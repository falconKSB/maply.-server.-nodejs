/**
 * Created by Sanchir on 2/16/2017.
 */
var apn = require('apn');


var apnProvider = new apn.Provider({
    token: {
        key: 'APNsAuthKey_C5D376D669.p8', // Path to the key p8 file
        keyId: 'ABCDE12345', // The Key ID of the p8 file (available at https://developer.apple.com/account/ios/certificate/key)
        teamId: 'ABCDE12345', // The Team ID of your Apple Developer Account (available at https://developer.apple.com/account/#/membership/)
    },
    production: false // Set to true if sending a notification to a production iOS app
});

var deviceToken = '5311839E985FA01B56E7AD74444C0157F7F71A2745D0FB50DED665E0E882';

module.exports.subscribe = function (deviceToken, deviceType) {
    /*CreatePlatformEndpointRequest createPlatformEndpointRequest = new CreatePlatformEndpointRequest();
    createPlatformEndpointRequest.setToken(device_token);
    if (device_type.equals(DeviceType.Ios)) {
        createPlatformEndpointRequest.setPlatformApplicationArn(Constant.IOS_ARN);
        CreatePlatformEndpointResult result = amazonSNS.createPlatformEndpoint(createPlatformEndpointRequest);
        System.out.println("Arn : " + result.getEndpointArn());
        return result.getEndpointArn();
    }

    return "";*/
};



module.exports.delete = function (endpointArn) {
    /* DeleteEndpointRequest deleteEndpointRequest = new DeleteEndpointRequest();
     deleteEndpointRequest.setEndpointArn(endpointArn);
     amazonSNS.deleteEndpoint(deleteEndpointRequest);*/
};

module.exports.sendIosNotification = function (targetArn, message, type, key, value, unReadCount, userId) {
    let notification = new apn.Notification();
    notification.topic = 'my.bundle.id';
    notification.expiry = Math.floor(Date.now() / 1000) + 3600;
    notification.badge = 3;
    notification.sound = 'ping.aiff';
    notification.alert = 'Hello World \u270C';
    notification.payload = {id: 123};

    apnProvider.send(notification, deviceToken).then(function(result) {
        // Check the result for any failed devices
        console.log(result);
    });
};