/**
 * Created by Sanchir on 2/16/2017.
 */
var userService = require('../services/user-service');
var User = require('../models/user').User;

module.exports.login = function(request) {
    return userService.findByEmailIgnoreCase(request.email)
        .then(function(user) {
            return user;
        });
};

module.exports.addAdmin = function(request) {
    let user = new User();
    user.name = request.name;
    return userService.findByEmailIgnoreCase(request.email)
        .then(function (found) {
        if(found == null)
        {
            user.email = request.email;
            user.password = request.password;
            user.role = 'ADMIN';
            user.isPublished = true;
            userService.save(user);

            let response = new User();
            response.messages = "Successfully Added";
            response.id = user.id;
            return response;
        }
    });
};

module.exports.updateAdmin = function(id, request) {
    let response = new User();
    return userService.findById(id).then(function(user) {
        if(request.name != null)
        {
            user.setName(request.name);
        }
        if(request.email != null)
        {
            userService.findByEmailIgnoreCase(request.email);
            user.email(request.email);
        }
        userService.save(user);
        response.messages = "Successfully Added";
        response.id = user.id;
        return response;
    });
};

module.exports.getAdminList = function(page, name) {
    let offset = 0;
    if (page > 0) {
        offset = (page - 1) * 10;
    }
    return User.find({'role' : 'ADMIN', 'isPublished' : true, 'name' : new RegExp('^'+name+'$', "i")})
        .then(function (users) {
        return users;
    });
};

module.exports.getAdminCount = function(name)  {
    return User.count({'role' : 'ADMIN', 'isPublished' : true, 'name' : new RegExp('^'+name+'$', "i")})
        .then(function (users) {
            return users;
        });
};

module.exports.getAdmin = function(id) {
    return User.findOne({'id' : id})
        .then(function (user) {
        return user;
    });
};

module.exports.changePassword = function(request) {
    return User.findOne({'id' : request.id})
        .then(function (user) {
            user.password = request.password;
            userService.save(user);
            user.message_id("Password Updated Successfully");
            return user;
    });
};