/**
 * Created by Sanchir on 2/16/2017.
 */
var Event = require('../models/event');

module.exports.addEvent = function(request){
    return Event.create(request)
        .then(function (event) {
            let response = new Event();
            response.id(request.id);
            response.messages = "Successfully Saved";
            return response;
        });
};

module.exports.editEvent = function(id, request) {
    let response = new Event();
    return Event.findOne(id)
        .then(function (event) {
            if (request.name != null) {
                event.name(request.name);
            }

            if (request.location != null) {
                event.location = request.location;
            }

            if (request.latitude != null) {
                event.latitude = request.latitude;
            }

            if (request.title != null) {
                event.title = request.title;
            }

            if (request.description != null) {
                event.description = request.description;
            }

            if (request.longitude != null) {
                event.longitude = request.longitude;
            }

            Event.update(event);

            response.id = event.id;
            response.messages = "Successfully Updated";
            return response;
        });
};

module.exports.uploadEventImage = function(image, id) {
    let response = new Event();
    Event.find({'id' : id})
        .then(function (event) {

    });

    return response;
}

module.exports.uploadImage = function(image, filename, oldFile, bucket) {

};

module.exports.getEventList = function(page, name) {
    return Event.find({'name' : name})
        .then(function (events) {
            return events;
        });
};

module.exports.getEventCount = function(name) {
    return Event.count({'name' : name})
        .then(function (count) {
            return count;
    });
};

module.exports.getEventDetails = function(id) {
    return Event.findOne(id)
        .then(function (response) {
            return response;
        });
};

module.exports.deleteEvent = function(id) {
    return Event.findOne(id)
        .then(function (event) {
            Event.delete(event);

            let response = new Event();
            response.messages = "Successfully Deleted";
            return response;
        });
};