/**
 * Created by Sanchir on 2/16/2017.
 */

var Journey = require('../models/journey');
var Message = require('../models/message');

var UserChat = require('../models/user-chat');


module.exports.getJourney = function(userId, offset) {
    let date = new Date();
    date.setDate(date.getHours() - 24);
};

module.exports.getJourneyMessage = function(journeyUserId, userId, offset) {
    let date = new Date();
    date.setDate(date.getHours() - 1);
    Message.find({'id': userId})
        .then(function (messages) {

    });
};

module.exports.autoDeleteJourney = function() {
    let date = new Date();
    date.setDate(date.getDate() + 1);
    return Journey.findAndUpdate({'isPublished' : true, 'createdDate' : {$lt : date}},
        {$set : {isPublished: false}}, function (err, journey) {
    });
};

module.exports.getMyJourney = function(userId) {
    let date = new Date();
    date.setDate(date.getDate() - 1);
    Message.findById(userId).exec(function (message) {
    });
};

module.exports.deleteMessage = function(messageId, userId) {
    Message.remove({_id : messageId});
};

module.exports.autoDeleteMessage = function() {
    return UserChat.findOneAndUpdate({'savedTill' :{ $gt : new Date()}}, {$set:{'isPublished' : false}}).exec(function (update) {

    });
};