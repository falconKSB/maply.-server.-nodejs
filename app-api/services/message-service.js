/**
 * Created by Sanchir on 2/16/2017.
 */
var User = require('../models/user');
var UserChat = require('../models/user-chat').UserChat;

var Message = require('../models/message').Message;
var Journey = require('../models/journey').Journey;

var MessageResponse = require('../responses/message-response');
var UserService = require('../services/user-service');


module.exports.addMessage = function(image, video, bannerImage, locationId, latitude,
                                     longitude, locationName, address, userId, isJounery, text) {
    let message = new Message();
    message.isPublished = true;
    message.latitude = latitude;
    message.viewCount = 0;
    message.longitude = longitude;
    message.location_address = address;
    message.text = text;
    message.location_id = locationId;
    message.location_name = locationName;

    return User.findById(userId)
        .then(function (user) {
        message.user_id = user;

        if(isJounery) {
            let journey = new Journey();
            journey.user_id = user._id;
            journey.location_id = locationId;
            journey.name = locationName;
            journey.latitude = latitude;
            journey.isPublished = true;
            journey.longitude = longitude;
            //journey.image = bannerImageFileName;
            journey.moments = 1;
            return Journey.create(journey);
        }
    })
    .then(function (journey) {
        message.journeyId = journey._id;
        return Message.create(message);
    })
    .then(function (message) {
        let response = new Message();
        response.id = message.id;
        response.messages = "Successfully Added ";
        return response;
    });
};

module.exports.sendMessage = function(messageId, request) {
    let userList = [];
    let messageCurrent;
    return Message.findById(messageId)
        .then(function (message) {
            messageCurrent = message;
            let arrayOfPromises = request.rows.map(function (id) {
                return User.findById(id);
            });
            return Promise.all(arrayOfPromises);
        }).then(function (arrayOfResults) {
            for(let i = 0; i < arrayOfResults.length; i++)
            {
                userList = userList.concat(arrayOfResults[i]);
            }
            for(let i =0; i < userList.length; i++)
            {
                let user = userList[i];
                let userChat = new UserChat();
                userChat.isUnread = true;
                userChat.isPublished = true;
                userChat.message_id = messageCurrent;
                userChat.user_id = user;
                userChat.user_by = messageCurrent.user_id;
                UserChat.create(userChat);
            }

            let response = new Message();
            response.messages = "Successfully Added ";
            return response;
        });
};

module.exports.findById = function(messageId) {
    return Message.findById(messageId)
        .then(function (message) {
            return message;
        })
};

module.exports.replyMessage = function(id, image, locationId, latitude, longitude,
                                       locationName, address, userId, text) {
    return User.findById(userId)
        .then(function (user) {
        return User.findById(id).then(function (friend) {
            if(image != null) {
                let message = new Message();
                message.isPublished = true;
                message.latitude = latitude;
                message.viewCount = 0;
                message.longitude = longitude;
                message.location_address = address;
                message.text = text;
                message.location_id = locationId;
                message.location_name = locationName;
                message.user_id = user;

                Message.create(message);

                let userChat = new UserChat();
                userChat.isUnread = true;
                userChat.isPublished = true;
                userChat.message_id = message;
                userChat.user_by = user;
                userChat.user_id = friend;

                UserChat.create(userChat);
            }

            let response = new Message();
            response.messages = "Successfully Added ";
            return response;
        })
    });
};

module.exports.getUnreadMessageCount = function (userId) {
    return UserService.findById(userId)
        .then(function(user) {
            return UserChat.count({"user_id" : user});
        })
        .then(function (count) {
            let response = new MessageResponse();
            response.count = count;
            return response;
        });
};

module.exports.getSentMessage = function (userId) {
};

module.exports.getDirectMessage = function (userId) {
};

module.exports.readMessage = function (ids, userId) {
};

module.exports.saveMessage = function (messageId, userId) {
    let messageIdInt = parseInt(messageId);
    let userIdInt = parseInt(userId);

    var date = new Date();
    date.setDate(date.getDate() + 1);

    UserChat.update({'savedTill' : new Date(), 'userId' : userIdInt, 'messageId': messageIdInt, 'isSaved' : true});
};