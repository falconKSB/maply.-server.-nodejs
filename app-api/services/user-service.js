/**
 * Created by Sanchir on 2/16/2017.
 */
var User = require('../models/user').User;
var UserChat = require('../models/user-chat').UserChat;
var Friend = require('../models/friend').Friend;
var Message = require('../models/message').Message;

var Notification = require('../models/notification');
var haversine = require('haversine-distance');
var FriendList = require('../responses/friend-list');

var notificationService = require('../services/notification-service');

module.exports.loginUser = function (request) {
    return User.findOne({'facebook_id': request.facebook_id}).then(function (user) {
        if(user != null)
        {
            if (request.device_token != null) {
                var token = notificationService.subscribe(request.device_token, request.device_type);
                if(!request.device_token.equals(user.device_token)) {
                    notificationService.delete(user.awsArn);
                }
                user.device_token = request.device_token;
                user.awsArn = token;

            } else if (user.device_token != null) {
                user.device_type = request.device_type;
            }

            if (user.email == null) {
                user.email = request.email;
            }

            if (user.gender == null) {
                user.gender = request.gender;
            }

            if (user.birthday == null) {
                user.birthday = request.birthday;
            }

            if (user.image == null) {
                user.image = request.image;
            }

            if (user.latitude == null) {
                user.latitude = request.latitude;
            }

            if (user.longitude == null) {
                user.longitude = request.longitude;
            }

            if (user.lang == null) {
                user.lang = "da";
            }

            if (user.name == null) {
                user.name = request.name;
            }

            else {
                user.lang = request.lang;
            }

            return User.update({'id' : user.id}, user)
                .then(function (funcResponse) {
                     return user;
                }).catch(function (error) {
                    console.log(error);
                });
        }

        else {
            user = new User();
            user.facebook_id = request.facebook_id;
            user.email = request.email;
            user.name = request.name;
            user.image = request.image;
            user.isPublished = true;
            user.status = "Live";
            user.deviceType = request.device_type;
            if (request.device_token != null) {
                let token = notificationService.subscribe(request.device_token, request.device_type);
                user.deviceToken = request.device_token;
                user.awsArn = token;
            }

            if (request.latitude != null) {
                user.latitude = request.latitude;
            }

            if (request.longitude != null) {
                user.longitude = request.longitude;
            }

            user.role = 'USER';

            if (request.lang == null) {
                user.lang = "da";
            }
            else {
                user.lang = request.lang;
            }

            let notification = new Notification;
            notification.friend_request = true;
            notification.llive_friends_nearby = true;
            notification.soundNotification = true;
            notification.live_request = true;
            notification.live_update = true;
            notification.maximum_distance = 2;
            notification.maply_news = true;
            notification.user_id = user.id;
            notification.message_id = true;

            Notification.create(notification);

            return User.create(user)
                .then(function (currentUser) {
                    return currentUser;
                }).catch(function (error) {
                console.log(error);
            });
        }
    });
};

module.exports.getNotification = function (userId) {
    return Notification.findOne({'userId': userId})
        .then(function(notification){
            return notification;
        })
};

module.exports.updateNotification = function (request) {
    return Notification.findOne({'userId': request.user_id})
        .then(function(notification){
            notification.friend_request = request.friend_request;
            notification.llive_friends_nearby = request.llive_friends_nearby;
            notification.sound = request.sound;
            notification.live_request = request.live_request;
            notification.live_update = request.live_update;
            notification.maximum_distance = request.maximum_distance;
            notification.maply_news = request.maply_news;
            notification.user_id = request.user_id;
            notification.sound = request.sound;
            notification.message_id = request.message_id;

            Notification.update(notification);

            return notification;
        });
};

module.exports.updateUser = function (request) {
     return User.findOne({'id' : request.id})
        .then(function (user) {
            if (request.username != null) {
                user.username = request.username;
            }

            if (request.name != null) {
                user.name = request.name;
            }

            if (request.email != null) {
                user.email = request.email;
            }

            if (request.status != null) {
                if(request.status == 'Away') {
                    let hours = request.offlineHours;
                    let activeTill = Date.now() + hours;

                    user.offlineHours = hours;
                    user.statusActiveTill = activeTill;
                } else if (request.status == 'Live') {
                    user.statusActiveTill = null;
                }
                user.status = request.status;
            }

             User.update(user);
             return user;
        });
};

module.exports.uploadFacebookFriends = function(userId, request) {
     return User.findOne({'id': userId, 'isPublished': true})
         .then(function (user) {
             if (user != null) {
                 let userList = [];
                 request.map(function (facebookId) {
                     return User.findOne({"facebook_id": facebookId})
                         .then(function (friend) {
                             if (friend != null) {
                                 return Friend.findOne({"user_by": user, "user": friend})
                                     .then(function (userFriend) {
                                         if (userFriend == null) {
                                              return Friend.findOne({"user_by": friend, "user": user})
                                                  .then(function (userForward) {
                                                      if (userForward == null) {
                                                          userForward = new Friend();
                                                          userForward.isFacebookFriend = true;
                                                          userForward.isInternalFriend = false;
                                                          userForward.user_by = user;
                                                          userForward.user = friend;
                                                          userForward.friend_request = 'None';
                                                          userForward.live_request = 'None';
                                                          Friend.create(userForward);
                                                      }
                                                  })
                                         }
                                     })
                             }
                             return userList;
                         })
                         .catch(function (err) {
                             console.log(err);
                         })
                 });
             }
         })
};

module.exports.findOne = function (userId) {
    return User.findOne({'user_id': userId, 'isPublished': true}
        .then(function (user) {
            return user;
        })
    )
};

module.exports.checkUserName  = function (userName) {
    return User.findOne({'name': userName, 'isPublished': true})
        .then(function (user) {
            return user == null;
        }).catch(function (err) {
            console.log(err);
        });
};

module.exports.uploadUserImage = function (image, id) {
};

module.exports.searchUser = function (userId, keyword, offset) {
    if(keyword != null)
    {
        return User.find({$text: {$search: keyword}, fields: {type: [String], text: true}}, function (err, results) {
            if (err) {
                console.log(err);
            } else {
                return results;
            }
        });
    }

    else {
        return User.find()
            .then(function (users) {
                return users;
            })
    }
};

module.exports.userAction = function(userId, request) {
    return User.findOne({'id': userId, 'isPublished': true})
        .then(function (userFinded) {
            if (userFinded != null) {
                var choosedFriend;
                request.map(function (item) {
                    User.findOne({'id': item.user_id, 'isPublished': true})
                        .then(function (friend) {
                            if (friend != null) {
                                choosedFriend = friend;
                                return Friend.findOne({"user_by": userFinded, "user": friend});
                            }
                        })
                        .then(function (userFriend) {
                            if (userFriend == null) {
                                userFriend = new Friend();
                            }
                            else if (item.action == 'Request' && userFriend.friend_request == 'None' ||
                                (item.action == 'LiveRequest' && userFriend == 'None')) {
                                userFriend.user_by = userFinded;
                                userFriend.user = choosedFriend;
                            }
                            if (item.action == 'Disconnect') {
                                userFriend.live_request = 'None';
                            } else if (item.action == 'Live' || item.action == 'LiveRequest') {
                                if (item.time != null) {
                                    var date = new Date();
                                    date.setDate(date.getDate() + 1);
                                    if (item.time == 0) {
                                        date.setDate(date.getFullYear() + 10)
                                    }
                                    userFriend.expiryDate = date;
                                    if (item.time == 24) {
                                        userFriend.live_request_type = 'H24';
                                    } else if (item.time == 72) {
                                        userFriend.live_request_type = 'H72';
                                    } else {
                                        userFriend.live_request_type = 'Until Disconnect';
                                    }
                                }
                                userFriend.live_request = item.action;
                            } else {
                                if (item.action == 'Request' && userFriend.friend_request != null
                                    && userFriend.friend_request == 'Request' && userFriend.user_by != null
                                    && userFriend.user_by.id != userFinded.id) {
                                    userFriend.friend_request = 'Friend';
                                    userFriend.isInternalFriend = true;
                                } else if (item.action == 'Decline') {
                                    userFriend.live_request = item.action;
                                } else {
                                    userFriend.isInternalFriend = false;
                                    userFriend.live_request = 'None';
                                    userFriend.friend_request = item.action;
                                }
                            }

                            if (userFriend.user_by == null) {
                                userFriend.user_by = userFinded;
                                userFriend.user = choosedFriend;
                            }

                            Friend.create(userFriend);

                            let response = new User();
                            response.message = "Successfully Updated";
                            return response;
                        });
                });
            }})
    };

module.exports.getBlockUser = function (userId, offset) {
    return User.find({'id' : userId})
        .then(function (users) {
            let arrayOfPromises = users.map(function (user) {
                return Friend.findOne({"user_by": user})
                    .then(function (friends) {
                        return friends;
                    })
            });
            return Promise.all(arrayOfPromises);
        })
        .then(function (arrayOfResults) {
            let users = [];
            for(let i = 0; i < arrayOfResults.length; i++)
            {
                users = users.concat(arrayOfResults[i]);
            }

            let usersWithoutUs = [];
            for(let j = 0; j < users.length; j++)
            {
                let user = users[j];
                if(user.id != userId) {
                    usersWithoutUs = usersWithoutUs.concat(user);
                }
            }
            return usersWithoutUs;
        });
};

module.exports.getFriendList = function (userId, offset, keyword) {
    return User.findOne({'id' : userId})
        .then(function (user) {
            return Friend.find({$or:[ {"user_by": user._id}, {"user": user._id}]});
        })
        .then(function(friends) {
            let arrayOfPromises = friends.map(function (friend) {
                return User.find({$or:[ {"_id": friend.user_by}, {"_id": friend.user}]})
            });
            return Promise.all(arrayOfPromises);
        })
        .then(function (arrayOfResults) {
            let users = [];
            for(let i = 0; i < arrayOfResults.length; i++)
            {
                users = users.concat(arrayOfResults[i]);
            }

            let usersWithoutUs = [];
            for(let j = 0; j < users.length; j++)
            {
                let user = users[j];
                if(user.id != userId) {
                    usersWithoutUs = usersWithoutUs.concat(user);
                }
            }
            return usersWithoutUs;
        });
};

module.exports.getFriendRequest = function (userId) {
    let liveRequest = [];
    let friendReqeust = [];
    let choosedUser;
    return User.findOne({'id': userId})
        .then(function (user) {
            choosedUser = user;
            return Friend.find({ $or:[ {"user_by": user}, {"user": user}]})
        })
        .then(function(friends) {

            if (friends.length  == 0)
            {
                let friendRequest = new FriendList();
                friendRequest.topFriends = null;
                friendRequest.friends = null;

                return friendRequest;
            }

            friends.forEach(function(row, i, arr) {
                if (row.live_request != null && row.live_request == 'LiveRequest') {
                    choosedUser.status_type = row.live_request;
                    choosedUser.live_type = row.live_request_type;
                    liveRequest.push(choosedUser);
                }
                else if(row.friend_request != null){
                    choosedUser.status_type = row.friend_request;
                    friendReqeust.push(choosedUser);
                }
            });

            let friendRequest = new FriendList;
            friendRequest.friendReqeust = friendReqeust;
            friendRequest.liveRequest = liveRequest;
            return friendRequest;
        });
};

module.exports.getTopFriendList = function (userId) {
    return User.findOne(userId)
        .then(function (user) {
            return Friend.find({"user_by": user, "user" : user});
        })
        .then(function(friends) {
            let arrayOfPromises = friends.rows.map(function (friend) {
                return User.findOne({ $or:[ {"user_by":user}, {"user":user}] })
                    .then(function (user) {
                        return user;
                    })
            });
            return Promise.all(arrayOfPromises);
        })
        .then(function (arrayOfResults) {
            let users = [];
            for(let i = 0; i < arrayOfResults.length; i++)
            {
                users = users.concat(arrayOfResults[i]);
            }

            let topFriends = users.slice(0, 2);
            let friends = users.slice(3, users.length - 1);

            let friendRequest = new FriendList();
            friendRequest.topFriends = topFriends;
            friendRequest.friends = friends;

            return friendRequest;
        });
};

module.exports.findById = function(userId) {
    let id = parseInt(userId);
    return User.findOne({'id': id, 'isPublished': true}
        .then(function (user) {
           return user;
        })
    );
};

module.exports.geNearby = function(userId, latitude, longitude) {
    let a = {latitude: latitude, longitude: longitude};
    return User.find().then(function (users) {
        let arrayOfPromises = users.map(function (user) {
            if (haversine(a, {latitude: user.latitude, longitude: user.longitude}) < 300) {
                return user;
            }
        });
        return Promise.all(arrayOfPromises);
    }).then(function (arrayOfResults) {
        let users = [];
        for(let i = 0; i < arrayOfResults.length; i++)
        {
            users = users.concat(arrayOfResults[i]);
        }
        return users;
    });
};

module.exports.getUserChat = function (id, userId, offset) {
    let newId = parseInt(id);
    let newUserId = parseInt(userId);
    return UserChat.find({ $or:[ {'id' : newId, 'userBy' : newUserId}, {'id' : newUserId, 'userBy' : newId}], 'isUnread' : true})
        .then(function (userChats) {
            let arrayOfPromises = userChats.rows.map(function (userChat) {
                return Message.findById(userId.message_id);
            });
            return Promise.all(arrayOfPromises);
        }).then(function (arrayOfResults) {
            let messages = [];
            for(let i = 0; i < arrayOfResults.length; i++)
            {
                messages = messages.concat(arrayOfResults[i]);
            }
            return messages;
        });
};


module.exports.updateUserPushToken = function (request) {
    return User.findOne({'_id': request.id, 'isPublished': true}
        .then(function (user) {
            let response = new User();
            response.messages = "Successfully Updated";
            return response;
        })
    );
};

module.exports.updateLocation = function (request) {
    return User.findOne({'id': request.id, 'isPublished': true})
        .then(function (user) {
            user.full_address = request.full_address;
            user.location = request.location;
            user.latitude = request.latitude;
            user.longitude = request.longitude;
            User.update(user);

            let response = new User();
            response.message = "Successfully Updated";
            return response;
        });
};


module.exports.updateBatteryStatus = function (request) {
    return User.findOne({'id': request.id, 'isPublished': true})
        .then(function (user) {
            user.battery_status = request.battery_status;
            User.update(user);

            let response = new User();
            response.message = "Successfully Updated";
            return response;
        });
};

module.exports.searchUserCount = function(userId, keyword) {
    if(keyword != null)
    {
        return User.count({$text: {$search: keyword}, fields: {type: [String], text: true}}, function (err, results) {
            if (err) {
                console.log(err);
            } else {
                return results;
            }
        });
    }

    else {
        return User.count()
            .then(function (count) {
                return count;
            })
    }
};

module.exports.save = function(user) {
    return User.create(user);
};

module.exports.getUserList = function(page, name) {
    return User.find({'role' : 'USER', 'isPublished' : true, 'name' : name})
        .then(function (users) {
            return users;
        })
};

module.exports.getUserCount = function(name) {
    return User.count({'role' : 'USER', 'isPublished' : true, 'name' : name})
        .then(function (count) {
            return count;
        })
};

module.exports.getUserChatCount = function(id, messageType, userId) {
    return UserChat.count({'userBy' : id, 'userId' : userId})
        .then(function (count) {
            return count;
        })
};

module.exports.getNearByEvent = function (userId, latitude, longitude) {
    let a = {latitude: latitude, longitude: longitude};
    return User.find().then(function (users) {
        let arrayOfPromises = users.rows.map(function (user) {
            if (haversine(a, {latitude: user.latitude, longitude: user.longitude}) < 300) {
                return user;
            }
        });
        return Promise.all(arrayOfPromises);
    }).then(function (arrayOfResults) {
        return arrayOfResults;
    });
};

module.exports.getUser = function (id) {
    return User.findOne({'id': id, 'isPublished': true}
        .then(function (user) {
            return user;
        })
    );
};

module.exports.getUsersToBeOnline = function () {
    return User.find({'status' : 'Away', 'statusActiveTill' : {$lt : Date.now()}})
        .then(function (users) {
            return users;
        });
};

module.exports.findByEmailIgnoreCase = function(email) {
    return User.find({'email' : email})
        .then(function (users) {
            return users;
        });
};

module.exports.getLastHour = function() {
    let today = new Date();
    let hourAgo = new Date(today.getTime() - (1000*60*60));
    return User.count({'registerTime' :  {$gt : hourAgo}})
        .then(function (response) {
            return response;
        });
};

module.exports.getLastDay = function() {
    let today = new Date();
    let lastDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    return User.count({'registerTime' : {$gt : lastDay}})
        .then(function (response) {
            return response;
        })
};

module.exports.getLastWeek = function() {
    let today = new Date();
    let lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
    return User.count({'registerTime' : {$gt : lastWeek}})
        .then(function (response) {
            return response;
        })
        .catch (function (err) {
            console.log(err);
        });
};

module.exports.getOnlineUsers = function() {
    return User.find({'status' : "Live"})
        .then(function (users) {
            return users;
        });
};





