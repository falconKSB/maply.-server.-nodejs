/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */

var mongoose = require("mongoose");

var journeySchema = mongoose.Schema({
    locationId: { type: String },
    image: { type: String },
    userId: { type: Number },
    longitude : {type: Number},
    name: { type: String },
    moments: { type: Number },
    isPublished : {type: Boolean}
});

module.exports = mongoose.model("Journey", journeySchema);
