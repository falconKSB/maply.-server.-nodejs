/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userChatSchema = mongoose.Schema({
    user_id: {type: Schema.Types.ObjectId, ref: 'User'},
    user_by: {type: Schema.Types.ObjectId, ref: 'User'},
    message_id: {type: Schema.Types.ObjectId, ref: 'Message'},
    isUnread : {type: Boolean},
    savedTill: { type: Date, default: Date.now },
    isSaved : {type: Boolean},
    isPublished : {type: Boolean}
});
var UserChat = mongoose.model("UserChat", userChatSchema);

module.exports = UserChat;