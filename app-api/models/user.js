/**
 * Created by Sanchir Kartiev on 2/14/17.
 */

var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var creds = require('../config/settings')['DATABASE'];

var connection = mongoose.createConnection(creds.uri);
autoIncrement.initialize(connection);

var StatusType = {
    None: 'None',
    Request: 'Request',
    Block: 'Block',
    Friend: 'Friend',
    Live: 'Live',
    LiveRequest: 'LiveRequest',
    Decline: 'Decline',
    Disconnect: 'Disconnect',
};

var Status = {
    Live: 'Live',
    Away: 'Away'
};

var DeviceType = {
    Ios: 'Ios',
    Android: 'Ios'
};

var Role = {
    USER: 'USER',
    ADMIN: 'ADMIN'
};

var liveType = {
    H24 : 'H24',
    H27: 'H72',
    UntilDisconnect : 'UntilDisconnect'
};

var userSchema = mongoose.Schema({
    username: { type: String},
    id: {type: Number},
    facebook_id: { type: String },
    image: { type: String },
    device_token: { type: String },
    device_type : {type: DeviceType},
    location: { type: String },
    full_address: { type: String },
    name: { type: String },
    email: { type: String },
    battery_status: { type: Number },
    lang: { type: String },
    awsArn: { type: String },
    isPublished: { type: Boolean },
    status: {type: Status},
    offlineHours: { type: Number },
    statusActiveTill: { type: Date, default: Date.now },
    latitude: { type: Number },
    longitude: { type: Number },
    badgeCount: { type: Number },
    statusType: {type : StatusType},
    liveType : {type: liveType},
    time: { type: Date, default: Date.now },
    description: { type: String },
    title: { type: String },
    type: { type: String },
    password: { type: String },
    role : {type: Role},
    gender: { type: String },
    birthday: { type: Date, default: Date.now },
    registerTime : {type: Date, default: Date.now()},
});


userSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

userSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
    return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.name,
        exp: parseInt(expiry.getTime() / 1000),
    }, "maply");
};

userSchema.plugin(autoIncrement.plugin, { model: 'User', field: 'id', startAt: 1 });

var User = mongoose.model('User', userSchema);
module.exports = { User : User};
