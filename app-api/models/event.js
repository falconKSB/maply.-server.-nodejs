/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */

var mongoose = require("mongoose");

var eventSchema = mongoose.Schema({
    name: { type: String },
    title: { type: String },
    latitude: { type: Number },
    longitude : {type: Number},
    location : {type: String},
    image : {type: String},
    description: {type: String}
});

eventSchema.methods.getImage = function(){
    if(this.image && !this.image.toLowerCase().startWith("http"))
    {
        //TODO To bucket url
        return  "https://s3.amazonaws.com/image-maply/" + this.image;
    }

    return this.image;
};

module.exports =  mongoose.model("Event", eventSchema);
