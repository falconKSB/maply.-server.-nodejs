/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var FriendRequest = {
    None: 'None',
    Request: 'Request',
    Block: 'Block',
    Friend: 'Friend',
    Live: 'Live',
    LiveRequest: 'LiveRequest',
    Decline: 'Decline',
    Disconnect: 'Disconnect',
};

var LiveRequestType = {
    H24: 'H24',
    H72: 'H72',
    UntilDisconnect: 'UntilDisconnect'
};


var friendSchema = Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    user_by: {type: Schema.Types.ObjectId, ref: 'User'},
    friend_request: {type : FriendRequest},
    live_request: {type : FriendRequest},
    live_request_type: {type : LiveRequestType},
    expiryDate: {type: Date, default: Date.now},
    isFacebookFriend: {type: Boolean},
    isInternalFriend: {type: Boolean}
});

var Friend =  mongoose.model("Friend", friendSchema);
module.exports = { Friend : Friend};
