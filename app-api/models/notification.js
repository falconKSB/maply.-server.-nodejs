/**
 * Created by Sanchir Kartiev  on 2/14/2017.
 */
var mongoose = require("mongoose");

var notificationSchema = mongoose.Schema({
    maply_news : {type: Boolean},
    message : {type : Boolean},
    friend_request : {type: Boolean},
    live_request : {type: Boolean},
    live_update : {type: Boolean},
    llive_friends_nearby : {type: Boolean},
    maximum_distance : {type: Number},
    user_id : {type: Number},
    soundNotification : {type: Boolean}
});

module.exports = mongoose.model("Notification", notificationSchema);