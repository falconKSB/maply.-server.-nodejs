/**
 * Created by Sanchir on 2/28/2017.
 */
function JourneyResponse(id, user, name, image, latitude, longitude, moments, isNew, createdDate) {
    this.id = id;
    this.user = user;
    this.name = name;
    this.image = image;
    this.latitude = latitude;
    this.longitude = longitude;
    this.moments = moments;
    this.isNew = isNew;
    this.createdDate = createdDate;
}

function JourneyResponse() {

}


module.exports = JourneyResponse;