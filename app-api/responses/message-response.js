/**
 * Created by Sanchir on 2/28/2017.
 */

function MessageResponse(count, message, user, image, text) {
    this.count = count;
    this.message = message;
    this.user = user;
    this.image = image;
    this.text = text;
}

function MessageResponse() {

}


module.exports = MessageResponse;