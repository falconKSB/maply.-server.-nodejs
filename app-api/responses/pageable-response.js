/**
 * Created by Sanchir on 2/28/2017.
 */

function PageableResponse(list, count, total) {
    this.list = list;
    this.count = count;
    this.total = total;
}

function PageableResponse() {

}


module.exports = PageableResponse;