/**
 * Created by Sanchir on 2/16/2017.
 */
var Event = require('../models/event');


module.exports.addEvent = function(request){
    let response = new Event();
    return Event.create(request)
        .then(function (event) {
            response.id(request.id);
            response.messages = "Successfully Saved";
            return response;
        });
};

module.exports.editEvent = function(id, request) {
    let response = new Event();
    return Event.findOne(id)
        .then(function (event) {
            if (request.name != null) {
                event.name(request.name);
            }

            if (request.location != null) {
                event.location = request.location;
            }

            if (request.latitude != null) {
                event.latitude = request.latitude;
            }

            if (request.title != null) {
                event.title = request.title;
            }

            if (request.description != null) {
                event.description = request.description;
            }

            if (request.longitude != null) {
                event.longitude = request.longitude;
            }

            Event.update(event);

            response.id = event.id;
            response.messages = "Successfully Updated";
            return response;
        });
};

module.exports.uploadEventImage = function(image, id) {
    var response = new Event();
    Event.find({'id' : id})
        .then(function (event) {

    });
    /*var event = eventRepository.findOne(id);
    if (event != null) {
        try {
            var filename = System.currentTimeMillis() + image.getOriginalFilename();
            this.uploadImage(image, filename, event.getImage(), Constant.BUCKET_NAME);
            event.image = filename;
            eventRepository.save(event);
            response.setImage(event.getImage());
        } catch (IOException e) {
            response.setMessages("Error to Upload Image " + e.getLocalizedMessage());
        }
    } else {
        response.setMessages("Event Id Incorrect !");

    }*/

    return response;
}

module.exports.uploadImage = function(image, filename, oldFile, bucket) {
    /*byte[] bytes = image.getBytes();
    File file = new File(filename);
    FileOutputStream fos = new FileOutputStream(file);
    fos.write(bytes);
    fos.close();

    TransferManager transferManager = new TransferManager(amazonS3);
    Upload upload = transferManager.upload(bucket, filename, file);
    if (!StringUtility.isNullOrEmpty(oldFile)) {
        amazonS3.deleteObject(bucket, oldFile);
    }

    while (!upload.isDone()) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            break;
        }
    }

    file.delete();
    return filename;*/
};

module.exports.getEventList = function(page, name) {
    return Event.find({'name' : name})
        .then(function (events) {
            return events;
        });
    /*String sql = " SELECT e.id, e.name, e.location, e.image, e.title FROM event e ";
    if (!StringUtility.isNullOrEmpty(name)) {
        sql += " WHERE e.name like '%" + name + "%' ";
    }

    sql += " ORDER BY e.id DESC LIMIT ?, ? ";
    List<Event> userList = this.jdbcTemplate.query(sql, new Object[] { offset, Constant.LIMIT },
    new RowMapper<Event>() {

    @Override
        public Event mapRow(ResultSet rs, int arg1) throws SQLException {

            Event event = new Event();
            event.setId(rs.getLong("id"));
            event.setName(rs.getString("name"));
            event.setLocation(rs.getString("location"));
            event.setTitle(rs.getString("title"));
            event.setImage(rs.getString("image"));
            return event;
        }

    });

    return userList;*/
};

module.exports.getEventCount = function(name) {
    return Event.count({'name' : name})
        .then(function (count) {
            return count;
    });
};

module.exports.getEventDetails = function(id) {
    return Event.findOne(id)
        .then(function (response) {
            return response;
        });
};

module.exports.deleteEvent = function(id) {
    return Event.findOne(id)
        .then(function (event) {
            Event.delete(event);

            var response = new Event();
            response.messages = "Successfully Deleted";
            return response;
        });
};