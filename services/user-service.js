/**
 * Created by Sanchir on 2/16/2017.
 */
var User = require('../models/user').User;
var UserChat = require('../models/user').UserChat;
var Friend = require('../models/friend').Friend;
var Notification = require('../models/notification');

var notificationService = require('../services/notification-service');

module.exports.loginUser = function (request) {
    return User.findOne({'facebook_id': request.facebook_id}).then(function (user) {
        if(user != null)
        {
            if (request.deviceToken != null) {
                var token = notificationService.subscribe(request.deviceToken, request.deviceType);
                if(!request.deviceToken.equals(user.deviceToken)) {
                    notificationService.delete(user.awsArn);
                }
                user.deviceToken = request.deviceToken;
                user.awsArn = token;

            } else if (user.deviceToken != null) {
                user.deviceType = request.deviceType;
            }

            if (user.email == null) {
                user.email = request.email;
            }

            if (user.gender == null) {
                user.gender = request.gender;
            }

            if (user.birthday == null) {
                user.birthday = request.birthday;
            }

            if (user.image == null) {
                user.image = request.image;
            }

            if (user.latitude == null) {
                user.latitude = request.latitude;
            }

            if (user.longitude == null) {
                user.longitude = request.longitude;
            }

            if (user.lang == null) {
                user.lang = "da";
            }

            if (user.name == null) {
                user.name = request.name;
            }

            else {
                user.lang = request.lang;
            }

            return User.update({'id' : user.id}, user)
                .then(function (funcResponse) {
                     return user;
                }).catch(function (error) {
                    console.log(error);
                });
        }

        else {
            user = new User();
            user.facebook_id = request.facebook_id;
            user.email = request.email;
            user.name = request.name;
            user.image = request.image;
            user.isPublished = true;
            user.status = "Live";
            user.deviceType = request.device_type;
            if (request.deviceToken != null) {
                var token = notificationService.subscribe(request.deviceToken, request.deviceType);
                user.deviceToken = request.deviceToken;
                user.awsArn = token;
            }

            if (request.latitude != null) {
                user.latitude = request.latitude;
            }

            if (request.longitude != null) {
                user.longitude = request.longitude;
            }

            user.role = 'USER';

            if (request.lang == null) {
                user.lang = "da";
            }
            else {
                user.lang = request.lang;
            }

            var notification = new Notification;
            notification.friendRequest = true;
            notification.liveFriendsNearby = true;
            notification.soundNotification = true;
            notification.liveRequest = true;
            notification.liveUpdate = true;
            notification.maixmumDistance = 2;
            notification.maplyNews = true;
            notification.userId = user.id;
            notification.message = true;

            Notification.create(notification);

            return User.create(user)
                .then(function (currentUser) {
                    return currentUser;
                }).catch(function (error) {
                console.log(error);
            });
        }
    });
};

module.exports.getNotification = function (userId) {
    Notification.findOne({'userId': userId})
        .then(function(notification){
            return notification;
        })
};

module.exports.updateNotification = function (request) {
    Notification.findOne({'userId': request.userId})
        .then(function(notification){
            notification.friendRequest = request.friendRequest;
            notification.liveFriendsNearby = request.liveFriendsNearby;
            notification.sound = request.sound;
            notification.liveRequest = request.liveRequest;
            notification.liveUpdate = request.liveUpdate;
            notification.maixmumDistance = request.maixmumDistance;
            notification.maplyNews = request.maplyNews;
            notification.userId = request.userId;
            notification.sound = request.sound;
            notification.message = request.message;

            Notification.update(notification);

            return notification;
        });
};

module.exports.updateUser = function (request) {
     return User.findOne({'id' : request.id})
        .then(function (user) {
            if (request.username != null) {
                user.username = request.username;
            }

            if (request.name != null) {
                user.name = request.name;
            }

            if (request.email != null) {
                user.email = request.email;
            }

            if (request.status != null) {
                if(request.status == 'Away') {
                    let hours = request.offlineHours;
                    let activeTill = Date.now() + hours;

                    user.offlineHours = hours;
                    user.statusActiveTill = activeTill;
                } else if (request.status == 'Live') {
                    user.statusActiveTill = null;
                }
                user.status = request.status;
            }

             User.update(user);
             return user;
        });
};

module.exports.uploadFacebookFriends = function(userId, request) {
    var id = parseInt(userId);
     User.findOne({'id': id, 'isPublished': true}).then(function (user) {
        let userList = [];
     request.reduce((promiseChain, item) => {
         return promiseChain.then(() => new Promise((resolve) => {
             return User.findOne({'facebook_id': item, 'isPublished': true}).then(function (friend) {
                 if(friend != null)
                 {
                     return Friend.findOne({'userBy' : user, 'userTo' : friend}).then(function (userFriend) {
                         if(userFriend != null)
                         {
                             userFriend.isFacebookFriend = true;
                             Friend.update(userFriend);
                             return userList;
                         }
                         else
                         {
                             return Friend.findOne({'userBy': friend, 'userTo': user}).then(function (userForward) {
                                 if (userForward == null) {
                                     userForward = new Friend();
                                     userForward.isFacebookFriend = true;
                                     userForward.isInternalFriend = false;
                                     userForward.userBy = user;
                                     userForward.userTo = friend;
                                     userForward.friendRequest = 'None';
                                     userForward.liveRequest = 'None';
                                     Friend.create(userForward);
                                     return userList;
                                 }
                             });
                         }
                         return userList;
                     })
                 }
         })
         .catch(function (error) {
             console.log(error);
         });
         return userList;
     }))
     }, Promise.resolve());
    });
};

module.exports.findOne = function (userId) {
    return User.findOne({'user_id': userId, 'isPublished': true}
        .then(function (user) {
            return user;
        })
    )
};

module.exports.checkUserName  = function (userName) {
    return User.findOne({'name': userName, 'isPublished': true})
        .then(function (user) {
            return user == null;
        }).catch(function (err) {
            console.log(err);
        });
};

module.exports.uploadUserImage = function (image, id) {
    //TODO Insert Amazon S3 service
    /*User userResponse = new User();
    User user = userRepository.findOne(id);
    if (user != null) {
        try {
            String filename = System.currentTimeMillis() + image.getOriginalFilename();
            this.uploadImage(image, filename, user.getImage(), Constant.BUCKET_NAME);
            user.setImage(filename);
            userRepository.save(user);
            userResponse.setImage(user.getImage());
        } catch (IOException e) {
            userResponse.setMessages("Error to Upload Image " + e.getLocalizedMessage());
        }
    } else {
        userResponse.setMessages("User Id Incorrect !");

    }

    return userResponse;*/
};

module.exports.searchUser = function (userId, keyword, offset) {
    User.find(
            { $text : { $search : keyword} }
        )
        .exec(function(err, results) {
            return results;
        });
    ///TODO integrate search like Full-text
    /*String sql = "";
    if (!StringUtility.isNullOrEmpty(keyword)) {
        sql =
            "select user.id, user.name, user.username, user.image from user where user.id <> ? and user.username like '%" + keyword + "%' and user.name like '%" + keyword + "%' and " +
            "user.id not in " +
            "(" +
            "	select user_to from friend where user_by = ? " +
            "									and is_internal_friend = true " +
            ") and " +
            "user.id not in " +
            "(" +
            "	select user_by from friend where user_to = ? " +
            "									and is_internal_friend = true " +
            ") order by user.name limit ?, ?";

    } else {

        sql =
            "select user.id,user.name,user.username, user.image from user where user.id <> ? and " +
            "user.id in " +
            "(" +
            "	select user_to from friend where user_by = ? " +
            "									and is_facebook_friend = true " +
            "									and is_internal_friend = false " +
            "									and (friend_request <> 'Friend' and friend_request <> 'Block') " +
            ") or " +
            "user.id in " +
            "(" +
            "	select user_by from friend where user_to = ? " +
            "									and is_facebook_friend = true " +
            "									and is_internal_friend = false " +
            "									and (friend_request <> 'Friend' and friend_request <> 'Block') " +
            ") order by user.name limit ?, ?";
    }

    List<User> userList = jdbcTemplate.query(sql, new Object[] { userId, userId, userId, offset, Constant.LIMIT },
    (rs, rowNum) -> {
        User user = new User();
    user.setName(rs.getString("name"));
    user.setUsername(rs.getString("username"));
    user.setId(rs.getLong("id"));
    user.setImage(rs.getString("image"));

    return user;
});

    if (userList != null && userList.size() > 0) {
        String sqlForFriendRequest =
            "select u.id from user u join friend f on (u.id = f.user_to and f.user_by = ? or u.id = f.user_by and f.user_to = ? ) " +
            "WHERE f.is_internal_friend = false and f.friend_request = 'Request'";

        List<Long> usersWithFriendRequest = jdbcTemplate.query(sqlForFriendRequest, new Object[] { userId, userId },
        (rs, rowNum) -> rs.getLong("id"));

        if (usersWithFriendRequest != null && usersWithFriendRequest.size() > 0) {
            for (User u : userList) {
                Long uid = u.getId();
                if (usersWithFriendRequest.contains(uid)) {
                    u.setStatusType(UserRequest.Request);
                }
            }
        }
    }

    return userList;*/
};

module.exports.userAction = function(userId, request) {
    let user = null;
    User.findOne({'id': userId, 'isPublished': true}
        .then(function (userFinded) {
            if(userFinded != null)
            {
                request.forEach(function(item, i, arr) {
                    User.findOne({'id': item.Id, 'isPublished': true}
                        .then(function (friend) {
                            if(friend != null)
                            {
                                User.findOne({'userBy' : userFinded, 'userTo' : friend})
                                    .then(function (userFriend) {
                                        if(userFriend == null) {
                                            
                                        }
                                    });
                            }
                        })
                    );
                });
            }

        })
    );

/*
 User user = userRepository.findByIdAndIsPublished(userId, true);
 if (user == null) {
 throw new AppException("Invalid Login user");
 }

 for (UserActionRequest userRequest : request) {
 User friend = userRepository.findByIdAndIsPublished(userRequest.getUserId(), true);
 if (friend == null) {
 throw new AppException("Inavlid UserId");
 }

 Friend userFriend = friendRepository.findByUserByAndUserTo(user, friend);
 if (userFriend == null) {
 userFriend = friendRepository.findByUserByAndUserTo(friend, user);
 }

 if (userFriend == null) {
 userFriend = new Friend();
 } else if ((userRequest.getAction() == UserRequest.Request
 && userFriend.getFriendRequest() == UserRequest.None)
 || (userRequest.getAction() == UserRequest.LiveRequest
 && userFriend.getLiveRequest() == UserRequest.None)) {
 userFriend.setUserBy(user);
 userFriend.setUserTo(friend);
 }
 if(userRequest.getAction() == UserRequest.Disconnect) {
 userFriend.setLiveRequest(UserRequest.None);
 }
 else if (userRequest.getAction() == UserRequest.Live || userRequest.getAction() == UserRequest.LiveRequest) {
 if (userRequest.getTime() != null) {
 Calendar calendar = Calendar.getInstance();
 calendar.setTime(new Date());
 calendar.add(Calendar.HOUR, userRequest.getTime());
 if(userRequest.getTime() == 0) {
 calendar.add(Calendar.YEAR, 10);
 }
 userFriend.setExpiryDate(calendar.getTime());
 if(userRequest.getTime() == 24) {
 userFriend.setLiveRequestType(Enumeration.LiveRequestType.H24);
 } else if (userRequest.getTime() == 72) {
 userFriend.setLiveRequestType(Enumeration.LiveRequestType.H72);
 } else {
 userFriend.setLiveRequestType(Enumeration.LiveRequestType.UntilDisconnect);
 }
 }

 userFriend.setLiveRequest(userRequest.getAction());
 } else {
 if (userRequest.getAction() == UserRequest.Request && userFriend.getFriendRequest() != null
 && userFriend.getFriendRequest() == UserRequest.Request && userFriend.getUserBy() != null
 && userFriend.getUserBy().getId() != user.getId()) {
 userFriend.setFriendRequest(UserRequest.Friend);
 userFriend.setInternalFriend(true);
 } else if (userRequest.getAction() == UserRequest.Decline) {
 userFriend.setLiveRequest(userRequest.getAction());
 } else {
 userFriend.setInternalFriend(false);
 userFriend.setLiveRequest(UserRequest.None);
 userFriend.setFriendRequest(userRequest.getAction());
 }
 }

 if (userFriend.getUserBy() == null) {
 userFriend.setUserBy(user);
 userFriend.setUserTo(friend);
 }

 friendRepository.save(userFriend);
 String name = "";
 if(user.getName() != null && user.getName().contains(" ")){
 name = user.getName().substring(0, user.getName().indexOf(" "));
 }
 if (userRequest.getAction() == UserRequest.Request) {
 String txtMsg = " har tilføjet dig som ven!";

 if (user.getLang().equals("en"))
 txtMsg = " added you as a friend";

 notificationService.sendIosNotification(friend.getAwsArn(), name + txtMsg,
 Enumeration.NotificationType.UMC, "user_id", user.getId(),
 userChatRepository.countByUserAndIsUnread(friend, true), user.getId());
 } else if (userRequest.getAction() == UserRequest.LiveRequest) {

 String txtMsg = " har sendt dig en LIVE anmodning";

 if (user.getLang().equals("en"))
 txtMsg = " sent you a LIVE request";

 notificationService.sendIosNotification(friend.getAwsArn(), name + txtMsg,
 Enumeration.NotificationType.UMC, "user_id", user.getId(),
 userChatRepository.countByUserAndIsUnread(friend, true), user.getId());
 }
 }

 User response = new User();
 response.setMessages("Successfully Updated");
 return response;*/
};

module.exports.getBlockUser = function (userId, offset) {
    var id = parseInt(userId);
    User.find({'id' : id})
        .then(function (users) {
            var arrayOfPromises = users.rows.map(function (user) {
                return Friend.findOne({'userBy' : user})
                    .then(function (friends) {
                        return friends;
                    })
            });
            return Promise.all(arrayOfPromises);
        })
        .then(function (arrayOfResults) {
            return arrayOfResults;
        });
};

module.exports.getFriendList = function (userId, offset, keyword) {
    /*String sql;
    List<Long> updateList = new ArrayList<Long>();
    List<User> userList = new ArrayList<User>();

    if (!StringUtility.isNullOrEmpty(keyword) && keyword.equals("TYnbR2g35qH567fK34")) {
        sql =
            "select u.id, u.name, u.username, u.image, u.status, u.latitude, u.longitude, u.full_address,u.battery_status, f.id as fid, f.live_request, f.live_request_type, f.friend_request ,f.expiry_date " +
            "from friend f inner join user u on ((u.id = f.user_to and f.user_by = ? or u.id = f.user_by and f.user_to = ? ) and f.is_internal_friend = true) " +
            "WHERE f.live_request = 'Live'";

        jdbcTemplate.query(sql, new Object[] { userId, userId }, (rs, rowNum) -> {
            return returnUser(updateList, userList, rs);
    });

        for (Long id : updateList) {
            Friend friend = friendRepository.findOne(id);
            friend.setLiveRequest(UserRequest.None);
            friendRepository.save(friend);
        }
    } else {
        sql = "select u.id, u.name, u.username, u.image, u.status, u.latitude, u.longitude, u.full_address,u.battery_status, f.id as fid, f.live_request, f.live_request_type, f.friend_request ,f.expiry_date from friend f inner join user u on ((u.id = f.user_to and f.user_by = ? or u.id = f.user_by and f.user_to = ? ) and f.is_internal_friend = true ) ";
        if (!StringUtility.isNullOrEmpty(keyword)) {
            sql += " WHERE ( username like '" + keyword + "%' or name like '" + keyword + "%')";
        }

        sql += " limit ?, ?";

        jdbcTemplate.query(sql, new Object[] { userId, userId, offset, 100000 }, (rs, rowNum) -> {
            User user = new User();
        user.setName(rs.getString("name"));
        user.setUsername(rs.getString("username"));
        user.setId(rs.getLong("id"));
        user.setImage(rs.getString("image"));
        if (rs.getString("live_request") == null) {
            user.setStatusType(UserRequest.None);
        } else {
            user.setStatusType(UserRequest.valueOf(rs.getString("live_request")));
            String liveRequestType = rs.getString("live_request_type");
            if(liveRequestType != null) {
                user.setLiveType(LiveRequestType.valueOf(liveRequestType));
            }
        }

        if (rs.getString("expiry_date") != null) {
            Timestamp timeStamp = rs.getTimestamp("expiry_date");
            user.setTime(timeStamp);
            if (user.getTime().before(new Date())) {
                updateList.add(rs.getLong("fid"));
                user.setStatusType(UserRequest.None);
            }
        }
        user.setLatitude(rs.getDouble("latitude"));
        user.setLongitude(rs.getDouble("longitude"));
        user.setFullAddress(rs.getString("full_address"));
        user.setBatteryStatus(rs.getFloat("battery_status"));
        user.setStatus(UserStatus.valueOf(rs.getString("status")));
        userList.add(user);
        return user;
    });

        for (Long id : updateList) {
            Friend friend = friendRepository.findOne(id);
            friend.setLiveRequest(UserRequest.None);
            friendRepository.save(friend);
        }
    }

    return userList;*/
};

module.exports.returnUser = function (updateList, userList, rs) {
    /*User user = new User();
    user.setName(rs.getString("name"));
    user.setUsername(rs.getString("username"));
    user.setId(rs.getLong("id"));
    user.setImage(rs.getString("image"));
    if (rs.getString("live_request") == null) {
        user.setStatusType(UserRequest.None);
    } else {
        user.setStatusType(UserRequest.valueOf(rs.getString("live_request")));
        String liveRequestType = rs.getString("live_request_type");
        if(liveRequestType != null) {
            user.setLiveType(LiveRequestType.valueOf(liveRequestType));
        }
    }

    if (rs.getString("expiry_date") != null) {
        Timestamp timeStamp = rs.getTimestamp("expiry_date");
        user.setTime(timeStamp);
        if (user.getTime().before(new Date())) {
            updateList.add(rs.getLong("fid"));
            user.setStatusType(UserRequest.None);
        }
    }
    user.setLatitude(rs.getDouble("latitude"));
    user.setLongitude(rs.getDouble("longitude"));
    user.setFullAddress(rs.getString("full_address"));
    user.setBatteryStatus(rs.getFloat("battery_status"));
    user.setStatus(UserStatus.valueOf(rs.getString("status")));
    userList.add(user);
    return user;*/
};

module.exports.getFriendRequest = function (userId) {
    /*FriendList friendRequest = new FriendList();
     String sql = "select u.id, u.name, u.username, u.image, u.status, f.live_request, f.live_request_type, f.friend_request from friend f inner join user u on (u.id = f.user_by and f.user_to = ? and (f.friend_request = 'Request' or f.live_request = 'LiveRequest'))";

     List<User> liveReqeust = new ArrayList<User>();
     List<User> friendReqeuset = new ArrayList<User>();
     jdbcTemplate.query(sql, new Object[] { userId }, (rs, rowNum) -> {
     User user = new User();
     user.setName(rs.getString("name"));
     user.setUsername(rs.getString("username"));
     user.setId(rs.getLong("id"));
     user.setImage(rs.getString("image"));
     if (rs.getString("live_request") != null
     && rs.getString("live_request").equals(UserRequest.LiveRequest.toString())) {
     user.setStatusType(UserRequest.valueOf(rs.getString("live_request")));
     user.setLiveType(LiveRequestType.valueOf(rs.getString("live_request_type")));
     liveReqeust.add(user);
     } else if (rs.getString("friend_request") != null) {
     user.setStatusType(UserRequest.valueOf(rs.getString("friend_request")));
     friendReqeuset.add(user);
     }

     return user;
     });

     friendRequest.setFriendReqeust(friendReqeuset);
     friendRequest.setLiveRequest(liveReqeust);
     return friendRequest;*/
};

module.exports.getTopFriendList = function (userId) {
    /*FriendList friendRequest = new FriendList();
    String sql = "select u.id, u.name, u.username, u.image, u.status, f.live_request from friend f inner join user u on ((u.id = f.user_to and f.user_by = ? or u.id = f.user_by and f.user_to = ? ) and f.is_internal_friend = true)";

    List<User> userList = jdbcTemplate.query(sql, new Object[] { userId, userId }, (rs, rowNum) -> {
        User user = new User();
    user.setName(rs.getString("name"));
    user.setUsername(rs.getString("username"));
    user.setId(rs.getLong("id"));
    user.setImage(rs.getString("image"));
    return user;
});

    List<User> topFriends = new ArrayList<User>();
    List<User> friends = new ArrayList<User>();
    int i = 0;
    for (User user : userList) {
        if (i++ > 3) {
            topFriends.add(user);
        } else {
            friends.add(user);
        }
    }

    friendRequest.setTopFriends(topFriends);
    friendRequest.setFriends(friends);
    return friendRequest;*/
};

module.exports.findById = function(userId) {
    return User.findOne({'id': userId, 'isPublished': true}
        .then(function (user) {
           return user;
        })
    );
};

module.exports.geNearby = function(userId, latitude, longitude) {
   /*
    String sql = "select u.id, u.name, u.full_address, u.username, u.image, u.location, u.latitude, u.longitude, ROUND(( 3959 * ACOS( COS( RADIANS( ? ) ) * COS( RADIANS( u.latitude ) ) * COS(RADIANS(u.longitude) - RADIANS( ? )) + SIN(RADIANS( ?)) * SIN( RADIANS(u.latitude)))),2) as distance  from user  u left join friend f on (((f.user_to = u.id and f.user_by = ?) or (f.user_by = u.id and f.user_to = ?)) and status = 'Live') where  f.live_request = 'Live' HAVING distance <  ? ";
    List<User> userList = jdbcTemplate.query(sql,
    new Object[] { latitude, longitude, latitude, userId, userId, Constant.NEAR_BY },
    (rs, rowNum) -> {
    User user = new User();
    user.setName(rs.getString("name"));
    user.setUsername(rs.getString("username"));
    user.setId(rs.getLong("id"));
    user.setType("USER");
    user.setImage(rs.getString("image"));
    user.setFullAddress(rs.getString("full_address"));
    user.setLocation(rs.getString("location"));
    user.setLatitude(rs.getDouble("latitude"));
    user.setLongitude(rs.getDouble("longitude"));
    return user;
    });

    return userList;
    */
};

module.exports.getUserChat = function (id, userId, offset) {
    /*List<Message> list = jdbcTemplate.query(
        "select uc.id, m.image, m.media,  m.text from user_chat uc inner join message m on (m.id = uc.message_id and ((uc.user_id = ? and uc.user_by = ?) or (uc.user_by = ? and uc.user_id = ?))) AND uc.is_unread = ture order by m.created_date desc limit ? , ? ",
        new Object[] { id, userId, id, userId, offset, Constant.LIMIT }, (rs, rowNum) -> {
        Message message = new Message();
    message.setId(rs.getLong("id"));
    if (rs.getString("media") != null) {
        message.setMedia(rs.getString("media"));
        if (message.getMedia().endsWith("gif")) {
            message.setType(MediaType.GIF);
        } else {
            message.setType(MediaType.VIDEO);
        }
    } else {
        message.setType(MediaType.IMAGE);
    }

    message.setImage(rs.getString("image"));
    message.setText(rs.getString("text"));
    return message;
});


    jdbcTemplate.update("update inbox set is_unread = false, modified_date = ? where user_id = ? and user_by = ? ",
        new Object[] { new Date(), id, userId });
    jdbcTemplate.update("update user_chat set is_unread = false, modified_date = ? where user_id = ? and user_by = ? ",
        new Object[] { new Date(), id, userId });
    return list;*/
};


module.exports.updateUserPushToken = function (request) {
    return User.findOne({'id': request.id, 'isPublished': true}
        .then(function (user) {
            if (request.deviceToken != null) {
                let token = notificationService.subscribe(request.deviceToken, request.deviceType);
                if(!request.deviceToken.equals(user.deviceToken)) {
                    notificationService.delete(user.awsArn);
                }
                user.deviceType = request.deviceType;
                user.deviceToken = request.deviceToken;
                user.awsArn = token;

                let response = new User();
                response.messages = "Successfully Updated";
                return response;
            }
        })
    );


};

module.exports.updateLocation = function (request) {
    return User.findOne({'id': request.id, 'isPublished': true}
        .then(function (user) {
            user.fullAddress = request.fullAddress;
            user.location = request.location;
            user.latitude = request.latitude;
            user.longitude = request.longitude;
            User.update(user);

            let response = new User();
            response.messages = "Successfully Updated";
            return response;
        })
    );
};


module.exports.updateBatteryStatus = function (request) {
    return User.findOne({'id': request.id, 'isPublished': true}
        .then(function (user) {
            user.batteryStatus = request.batteryStatus;
            User.update(user);

            let response = new User();
            response.messages = "Successfully Updated";
            return response;
        })
    );
};

module.exports.searchUserCount = function(userId, keyword) {

    /*String sql = "";
    if (!StringUtility.isNullOrEmpty(keyword)) {
        sql = "select count(*) from user u left join friend f on ((f.user_to = u.id and f.user_by = ?) or
        (f.user_by = u.id and f.user_to = ?)) where  friend_request <> 'Block' and friend_request <> 'Friend' and (username like '"
            + keyword + "%' or  name like '" + keyword + "%' )";
    } else {
        sql = "select count(*) from friend f inner join user u on ((f.user_to = u.id and f.user_by = ?) or
        (f.user_by = u.id and f.user_to = ?)) where f.is_facebook_friend = true and friend_request <> 'Block' and friend_request <> 'Friend'";
    }

    List<Long> count = jdbcTemplate.queryForList(sql, new Object[] { userId, userId }, Long.class);
    return count != null && count.size() > 0 ? count.get(0) : 0L;*/
};

module.exports.save = function(user) {
    return User.create(user);
};

module.exports.getUserList = function(page, name) {
    return User.find({'role' : 'USER', 'isPublished' : true, 'name' : name})
        .then(function (users) {
            return users;
        })
};
module.exports.getUserCount = function(name) {
    return User.count({'role' : 'USER', 'isPublished' : true, 'name' : name})
        .then(function (count) {
            return count;
        })
};

module.exports.getUserChatCount = function(id, messageType, userId) {
    return UserChat.count({'userBy' : id, 'userId' : userId})
        .then(function (count) {
            return count;
        })
};

module.exports.getNearByEvent = function (userId, latitude, longitude) {

};

module.exports.getUser = function (id) {
    return this.findById(id);
};

module.exports.getUsersToBeOnline = function () {
    return User.find({'status' : 'Away', 'status_active_till' : {$lt : Date.now()}})
        .then(function (users) {
            return users;
        });
};

module.exports.findByEmailIgnoreCase = function(email) {
    return User.find({'email' : email})
        .then(function (users) {
            return users;
        });
};



