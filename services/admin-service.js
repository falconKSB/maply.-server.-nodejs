/**
 * Created by Sanchir on 2/16/2017.
 */
var userService = require('../services/user-service');

module.exports.login = function(request) {
    return userService.findByEmailIgnoreCase(request.email)
        .then(function(user) {
            return user;
        });
};

module.exports.addAdmin = function(request) {
    var user = new User();
    user.name = request.name;
    return userService.findByEmailIgnoreCase(request.email).then(function (found) {
        if(found == null)
        {
            user.email = request.email;
            user.password = request.password;
            user.role = 'ADMIN';
            user.isPublished = true;
            userService.save(user);

            var response = new User();
            response.messages = "Successfully Added";
            response.id = user.id;
            return response;
        }
    });
};

module.exports.updateAdmin = function(id, request) {
    var response=new User();
    return userService.findById(id).then(function(user) {
        if(request.name != null)
        {
            user.setName(request.name);
        }
        if(request.email != null)
        {
            userService.findByEmailIgnoreCase(request.email);
            user.email(request.email);
        }
        userService.save(user);
        response.messages = "Successfully Added";
        response.id = user.id;
        return response;
    });
};

module.exports.getAdminList = function(page, name) {
    var offset = 0;
    if (page > 0) {
        offset = (page - 1) * Constant.LIMIT;
    }

    /*String sql =" SELECT u.id, u.name, u.email FROM user u WHERE role='ADMIN' AND u.is_published=true ";
    if(!StringUtility.isNullOrEmpty(name))
    {
        sql+=" AND  u.name like '%"+name+"%' ";
    }

    sql+=" ORDER BY u.id DESC LIMIT ?, ? ";
    List<User> userList = this.jdbcTemplate.query(sql, new Object[] { offset, Constant.LIMIT },
    new RowMapper<User>() {

    @Override
        public User mapRow(ResultSet rs, int arg1) throws SQLException {

            User user=new  User();
            user.setId(rs.getLong("id"));
            user.setName(rs.getString("name"));
            user.setEmail(rs.getString("email"));
            return user;
        }

    });

    return userList;*/
};

module.exports.getAdminCount = function(name)  {

    /*String sql =" SELECT COUNT(*) AS count FROM user u WHERE role='ADMIN'  AND u.is_published=true ";
    if(!StringUtility.isNullOrEmpty(name))
    {
        sql+=" AND u.name like '%"+name+"%' ";
    }
    List<Long> count = jdbcTemplate.queryForList(sql, new Object[] {}, Long.class);
    return count != null && count.size() > 0 ? count.get(0) : 0L;*/
};

module.exports.getAdmin = function(id) {
    userService.findById(id).then(function (user) {
        return user;
    });
};

module.exports.changePassword = function(request) {
    userService.findById(request.id).then(function (user) {
        user.password = request.password;
        userService.save(user);
        user.message("Password Updated Successfully");
        return user;
    });
};